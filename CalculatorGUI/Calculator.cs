﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/* 3/28/18 - PEMDAS precedence support fully implemented along with negative number support.
 * 3/30/18 - Cleaned up program, removed unnecessary statements, functions, variables, and loops.
 * 4/1/18  - Root, Trig, Log, implicit multiplication, and other support added.
 * 4/1/18  - Mod support added.
 * 4/10/18 - Pretty much full support for a lot of expressions. Works great as far as I can tell.
 *           Also added decent comments.
 * */


namespace CalculatorGUI
{
    public partial class frmCalc : Form
    {
        // A is answer from previous equation and deg is used for degrees or radians.
        string A;
        bool deg = true;

        public frmCalc()
        {
            InitializeComponent();
        }

        private void solve_Click(object sender, EventArgs e)
        {
            string input = inputBox.Text;

            string operators = "^*/%+-";

            double operandOne = 0;
            double operandTwo = 0;
            string operand = "";

            int @decimal = 0;
            int parOpen = 0;
            int parClose = 0;
            int parCount = 0;
            int curOp = 0;
            int indexCurOp = 0;
            int indexPrecOp = 0;
            int switchPrecOp = 0;
            int startReplace = 0;
            int endReplace = 0;
            int lenReplace = 0;
            int subLen = 0;
            int operandCounter = 0;
            int tempPastParClose = 0;


            // Count parentheses pairs and return if non-matching.
            for (int y = 0; y < input.Length; y++)
            {
                if (input[y] == '(')
                {
                    parCount++;
                }
                else if (input[y] == ')')
                {
                    parCount--;
                }
            }

            if (parCount != 0)
            {
                warningLbl.Text = "Non-Matching Parentheses";
                return;
            }
            else
            {
                warningLbl.Text = "";
            }

            // Remove spaces and replace e and Pi with respective numbers.
            // Always put e and Pi in a pair of parentheses.
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == ' ')
                {
                    input = input.Remove(i, 1);
                    i--;
                }
                else if (input[i] == 'e')
                {
                    input = input.Remove(i, 1);
                    input = input.Insert(i, '(' + Convert.ToString(Math.E) + ')');
                    i--;
                }
                else if (input[i] == 'P' && input[i + 1] == 'i')
                {
                    input = input.Remove(i, 2);
                    input = input.Insert(i, '(' + Convert.ToString(Math.PI) + ')');
                    i--;
                }
            }


            // -(-8/-7+-6--5*-4^3)+-(-8/-7+-6--5*-4^3) --> I use this to test things.
            // Easier to copy/paste.

            // Loop is pretty much just for finding the operator in the input.
            // And for kepping the calculation running until we solve the whole equation.
            // Most of the time I look for the operator we are currently on in 'operators' string.
            // Some statements change the input/output string and we just reset indexCurOp and 
            // curOp (current operator in 'operators') to 0 and keep looping until it hits a condition
            // that checks if there are any operators at all in the input/output string.
            for (indexCurOp = 0; indexCurOp < input.Length; indexCurOp++)
            {
                // Left to right. Find first closing parenthesis, if it doesn't exist, we handle that.
                parClose = input.IndexOf(Convert.ToString(')'));
                parOpen = parClose;

                // If input string has parentheses at any time, we run this statement and don't really
                // ever go past it until all pairs are removed/solved.
                // Just continue the for loop after we make changes to input/output.
                if (parOpen >= 0)
                {
                    // Iterate backwards through input until it finds an opening parenthesis.
                    while (input[parOpen] != '(')
                    {
                        parOpen--;
                    }

                    // Just to make sure the while loop didn't mess up.
                    if (parOpen >= 0)
                    {
                        int iterateOp = 0;
                        indexCurOp = -1;

                        // Loop through each operator in 'operators' until we find one at an index inside
                        // the parentheses pair. We start at index 0 for 'operators' because the string
                        // is written with higher precedence on left.
                        while ((indexCurOp < 0 || indexCurOp < parOpen || indexCurOp > parClose)
                                    && iterateOp < 6)
                        {
                            indexCurOp = input.IndexOf(Convert.ToString(operators[iterateOp]), parOpen);

                            // If we get to this statement, it didn't find any other operators besides '-'
                            // so if it is a negative sign and not a minus sign, we check for any minus
                            // past it. If we don't find one at all or if it is past the current
                            // parentheses pair, indexCurOp is set to -1 and we handle that later.
                            if (iterateOp == 5 && indexCurOp > parOpen && indexCurOp < parClose)
                            {
                                if (input[indexCurOp - 1] == '(')
                                {
                                    indexCurOp = input.IndexOf(Convert.ToString(operators[5]), indexCurOp + 1);

                                    if (indexCurOp >= 0 && indexCurOp > parOpen + 1 && indexCurOp > parClose)
                                    {
                                        indexCurOp = -1;
                                    }
                                }
                            }

                            iterateOp++;
                        }


                        // If we find any operator inside the parentheses, decrement iterateOp
                        // because it will be 1 number too high from previous while. Then set
                        // curOp to the same operator as indexCurOp, not the same index, but the
                        // same operator in their respective strings.

                        /* If iterateOp isn't the same index as '-' in 'operators', we have to handle
                         * precedence. In other words, don't check precedence if we are looking at '-'.
                         * Because we handle negatives in other statements and loop through 'operators'
                         * and handle precedence of '+' anyway.
                         * If iterateOp is index of '^' we don't have to deal with precedence.
                         * 
                         * GetPrec() checks for precedence of other same level operators.
                         */
                        if (indexCurOp >= 0 && indexCurOp < parClose && indexCurOp > parOpen)
                        {
                            iterateOp--;
                            curOp = operators.IndexOf(input[indexCurOp]);

                            if (iterateOp < 5 && operators[iterateOp] != '^')
                            {
                                GetPrec(ref parOpen, ref parClose, ref input, ref indexPrecOp,
                                    operators, ref curOp, ref indexCurOp, ref switchPrecOp);
                            }
                        }


                        // At this point, if we searched for the best operator and didn't find it
                        // or it's outside the parentheses and we have tested every operator (iterateOp)
                        // we know we just need to check for implicit multiplication, negatives, and
                        // functions and solve them.
                        if ((indexCurOp < 0 || indexCurOp < parOpen || indexCurOp > parClose) &&
                                iterateOp >= 6)
                        {
                            int tempIndex = parOpen;
                            tempIndex++;

                            while (tempIndex < parClose + 1 && (Char.IsDigit(input[tempIndex]) ||
                                    input[tempIndex] == '.'))
                            {
                                tempIndex++;
                            }

                            if (((parOpen > 0 && (input[parOpen - 1] == '>' || input[parOpen - 1] == '-'))
                                && (parClose + 1 < input.Length && input[parClose + 1] == '<')))
                            {
                                input = input.Remove(parClose, 1);
                                input = input.Remove(parOpen, 1);

                                indexCurOp = -1;
                                curOp = 0;
                                continue;
                            }
                            
                            // Need to review this *********************
                            else if (tempIndex == parClose)
                            {
                                if ((parOpen > 0 && parClose + 1 < input.Length) &&
                                    ((input[parOpen - 1] == '>' && input[parClose + 1] == '<') ||
                                    input[parOpen - 1] == 'b'))
                                {
                                    input = input.Remove(parClose, 1);
                                    input = input.Remove(parOpen, 1);

                                    indexCurOp = -1;
                                    curOp = 0;
                                    continue;
                                }
                            }

                            // If char to left of parOpen is a function or digit.
                            // Anything we do in this statement results in resetting index variables
                            // and looping again. Each of these statements does different calculations
                            // and replaces different lengths in the input/output string.
                            if ((parOpen > 0 && (input[parOpen - 1] == '>' || input[parOpen - 1] == '<' ||
                                input[parOpen - 1] == 'g' || input[parOpen - 1] == 'n' || input[parOpen - 1] == 's' ||
                                Char.IsDigit(input[parOpen - 1]) || input[parOpen - 1] == ')')) ||
                                (parClose + 1 < input.Length && (Char.IsDigit(input[parClose + 1]) ||
                                                                    input[parClose + 1] == '(')))
                            {
                                bool operandGot = false; // If we already store a substr in 'operand' set to true
                                                         // so we don't replace it later on.

                                startReplace = 0;

                                tempPastParClose = parClose;


                                // nth root statement. 
                                // Find start of nth root syntax '>' and
                                // store the root. If root is even number and other operand is negative
                                // return warning. Otherwise just take nth root of number inside parentheses.

                                if (parOpen > 0 && !operandGot && input[parOpen - 1] == '<')
                                {
                                    operandCounter = parOpen - 1;

                                    while (input[operandCounter] != '>')
                                    {
                                        operandCounter--;
                                    }

                                    operandCounter++;

                                    operandOne = Convert.ToDouble(input.Substring(operandCounter, ((parOpen - 1) - operandCounter)));

                                    if (operandOne % 2 == 0 && input[parOpen + 1] == '-')
                                    {
                                        warningLbl.Text = "Can't take even root of negative number.";
                                        return;
                                    }
                                    else
                                    {
                                        operandTwo = Convert.ToDouble(input.Substring(parOpen + 1, (parClose - parOpen - 1)));

                                        operand = Convert.ToString(Math.Pow(Math.Abs(operandTwo), (1.0 / operandOne)));

                                        if (operandTwo < 0)
                                        {
                                            operand = Convert.ToString(Convert.ToDouble(operand) / -1);
                                        }

                                        startReplace = operandCounter - 1;
                                    }

                                    operandGot = true;
                                }

                                // Square root.
                                // If operand is negative, return warning.
                                // Otherwise replace square root of operandTwo in string.
                                else if (parOpen > 0 && !operandGot && input[parOpen - 1] == '>')
                                {
                                    operandTwo = Convert.ToDouble(input.Substring((parOpen + 1), (parClose - parOpen - 1)));

                                    if (input[parOpen - 2] != '<')
                                    {
                                        warningLbl.Text = "Incorrect format";
                                        return;
                                    }

                                    if (operandTwo < 0)
                                    {
                                        warningLbl.Text = "Can't take square root of negative number.";
                                        return;
                                    }

                                    operand = Convert.ToString(Math.Sqrt(operandTwo));

                                    startReplace = parOpen - 2;

                                    operandGot = true;
                                }

                                // Gets the base of a log. Syntax is 'logb(b)(x)' -- parentheses required
                                // for equations within pair, otherwise if just a number, not required.
                                if ((parOpen > 0 && Char.IsDigit(input[parOpen - 1])) || (parOpen > 1 && Char.IsDigit(input[parOpen - 2])))
                                {
                                    if (Char.IsDigit(input[parOpen - 1]))
                                    {
                                        operandCounter = parOpen - 1;
                                    }
                                    else if (input[parOpen - 1] == ')')
                                    {
                                        operandCounter = parOpen - 2;
                                    }

                                    while (operandCounter >= 0 && (Char.IsDigit(input[operandCounter]) || input[operandCounter] == '.'))
                                    {
                                        operandCounter--;
                                    }

                                    if (operandCounter >= 0 && input[operandCounter] != '(')
                                    {
                                        operandCounter++;
                                    }

                                    if (operandCounter > 0 && input[operandCounter - 1] == 'b')
                                    {
                                        operandOne = Convert.ToDouble(input.Substring(operandCounter, parOpen - operandCounter));
                                    }
                                }

                                // log/ln statement.
                                if (!operandGot && ((parOpen > 1 && (input[parOpen - 1] == 'g' ||
                                        (parOpen > 1 && input[parOpen - 1] == 'n' && input[parOpen - 2] == 'l')))
                                            || (operandCounter > 0 && input[operandCounter - 1] == 'b')))
                                {
                                    // Operand inside parentheses.
                                    operandTwo = Convert.ToDouble(input.Substring(parOpen + 1, (parClose - parOpen - 1)));

                                    // Can't log of negative.
                                    if (operandTwo < 0)
                                    {
                                        warningLbl.Text = "Incorrect log operand";
                                        return;
                                    }

                                    // Solve log with base.
                                    if ((Char.IsDigit(input[parOpen - 1])))
                                    {
                                        if (operandCounter > 0 && input[operandCounter - 1] == 'b')
                                        {
                                            operand = Convert.ToString(Math.Log(operandTwo, operandOne));

                                            startReplace = operandCounter - 4;
                                        }
                                    }

                                    // Solve natural log.
                                    else if (parOpen > 1 && input[parOpen - 1] == 'n' && input[parOpen - 2] == 'l')
                                    {
                                        operand = Convert.ToString(Math.Log(operandTwo));

                                        startReplace = parOpen - 2;
                                    }

                                    // Solve log10.
                                    else if (input[parOpen - 1] == 'g')
                                    {
                                        operand = Convert.ToString(Math.Log10(operandTwo));

                                        startReplace = parOpen - 3;
                                    }

                                    operandGot = true;
                                }

                                // sin/cos/tan.
                                else if (!operandGot && (parOpen > 1 && (input[parOpen - 1] == 'n' ||
                                            input[parOpen - 1] == 's')))
                                {
                                    operandTwo = Convert.ToDouble(input.Substring((parOpen + 1), (parClose - parOpen - 1)));

                                    // sin or tan.
                                    if (input[parOpen - 1] == 'n')
                                    {
                                        // sin.
                                        // Support for degrees or radians.
                                        if (input[parOpen - 2] == 'i')
                                        {
                                            if (deg == true)
                                            {
                                                if (parOpen > 3 && input[parOpen - 4] == 'a')
                                                {
                                                    if (operandTwo >= -1 && operandTwo <= 1)
                                                    {
                                                        operand = Convert.ToString(Math.Round(Math.Asin(operandTwo) * 180 / Math.PI, 10));
                                                    }
                                                    else
                                                    {
                                                        warningLbl.Text = "Can't take inverse sin of " + operandTwo;
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Sin(operandTwo * Math.PI / 180), 10));
                                                }
                                            }
                                            else
                                            {
                                                if (parOpen > 3 && input[parOpen - 4] == 'a')
                                                {
                                                    if (operandTwo >= -1 && operandTwo <= 1)
                                                    {
                                                        operand = Convert.ToString(Math.Round(Math.Asin(operandTwo), 10));
                                                    }
                                                    else
                                                    {
                                                        warningLbl.Text = "Can't take inverse sin of " + operandTwo;
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Sin(operandTwo), 10));
                                                }
                                            }
                                        }

                                        // tan.
                                        // Degrees or radians.
                                        else if (input[parOpen - 2] == 'a')
                                        {
                                            if (deg == true)
                                            {
                                                if (parOpen > 3 && input[parOpen - 4] == 'a')
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Atan(operandTwo) * 180 / Math.PI, 10));
                                                }
                                                else
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Tan(operandTwo * Math.PI / 180), 10));
                                                }
                                            }
                                            else
                                            {
                                                if (parOpen > 3 && input[parOpen - 4] == 'a')
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Atan(operandTwo), 10));
                                                }
                                                else
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Tan(operandTwo), 10));
                                                }
                                            }
                                        }

                                        operandGot = true;
                                    }

                                    // cos.
                                    // Degrees or radians.
                                    else if (input[parOpen - 1] == 's')
                                    {
                                        operandTwo = Convert.ToDouble(input.Substring((parOpen + 1), (parClose - parOpen - 1)));

                                        if (deg == true)
                                        {
                                            if (parOpen > 3 && input[parOpen - 4] == 'a')
                                            {
                                                if (operandTwo >= -1 && operandTwo <= 1)
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Acos(operandTwo) * 180 / Math.PI, 10));
                                                }
                                                else
                                                {
                                                    warningLbl.Text = "Can't take inverse cos of " + operandTwo;
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                operand = Convert.ToString(Math.Round(Math.Cos(operandTwo * Math.PI / 180), 10));
                                            }
                                        }
                                        else
                                        {
                                            if (parOpen > 3 && input[parOpen - 4] == 'a')
                                            {
                                                if (operandTwo >= -1 && operandTwo <= 1)
                                                {
                                                    operand = Convert.ToString(Math.Round(Math.Acos(operandTwo), 10));
                                                }
                                                else
                                                {
                                                    warningLbl.Text = "Can't take inverse cos of " + operandTwo;
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                operand = Convert.ToString(Math.Round(Math.Cos(operandTwo), 10));
                                            }
                                        }

                                        operandGot = true;
                                    }


                                    // Starting index of replacement. If (index - 1) is 'a', decrement.
                                    startReplace = parOpen - 3;

                                    if (parOpen > 3 && input[parOpen - 4] == 'a')
                                    {
                                        startReplace--;
                                    }
                                }


                                // Implicit multiplication.
                                if (!operandGot && ((startReplace > 0 && Char.IsDigit(input[startReplace - 1])) ||
                                    (parOpen > 0 && (Char.IsDigit(input[parOpen - 1]) || input[parOpen - 1] == ')')) ||
                                    (parClose + 1 < input.Length && (Char.IsDigit(input[parClose + 1]) ||
                                    input[parClose + 1] == '('))))
                                {
                                    int endImplicitOp = 0;
                                    startReplace = 0;
                                    lenReplace = 0;

                                    // Get operand inside parentheses for implicit multiplication.
                                    if (parOpen > 0 && (Char.IsDigit(input[parOpen - 1]) || input[parOpen - 1] == ')'))
                                    {
                                        operandTwo = Convert.ToDouble(input.Substring(parOpen + 1, (parClose - parOpen - 1)));
                                    }

                                    // Left to right, if left of parOpen is digit, store the number for use in operandOne.
                                    if (parOpen > 0 && Char.IsDigit(input[parOpen - 1]))
                                    {
                                        startReplace = parOpen;
                                        endImplicitOp = startReplace;
                                        startReplace--;

                                        while ((startReplace >= 0 && (Char.IsDigit(input[startReplace]) ||
                                        input[startReplace] == '.')) || (startReplace > 0 && input[startReplace] == '-' &&
                                        (!Char.IsDigit(input[startReplace - 1]) && input[startReplace - 1] != ')')))
                                        {
                                            startReplace--;
                                        }

                                        startReplace++;

                                        operandOne = Convert.ToDouble(input.Substring(startReplace, endImplicitOp - startReplace));
                                    }

                                    // No digit to left, store number in parentheses as operandOne.
                                    // If to right of parClose is parenthesis, remove parentheses from operandOne.
                                    // After we do that, program should loop and remove any pairs around the second operand
                                    // we want to get. Then come back here and iterate through string until it gets operandTwo.
                                    else if (parClose + 1 < input.Length && (Char.IsDigit(input[parClose + 1]) || input[parClose + 1] == '('))
                                    {
                                        operandOne = Convert.ToDouble(input.Substring(parOpen + 1, parClose - parOpen - 1));

                                        startReplace = parOpen;

                                        tempPastParClose++;

                                        if (iterateOp >= 6 && input[parClose + 1] == '(')
                                        {
                                            input = input.Remove(parClose, 1);
                                            input = input.Remove(parOpen, 1);

                                            indexCurOp = -1;
                                            curOp = 0;
                                            continue;
                                        }

                                        if (input[tempPastParClose] == '(')
                                        {
                                            tempPastParClose++;
                                        }

                                        while (tempPastParClose < input.Length && (Char.IsDigit(input[tempPastParClose]) ||
                                                input[tempPastParClose] == '.' || (input[tempPastParClose] == '-' &&
                                                !Char.IsDigit(input[tempPastParClose - 1]))))
                                        {
                                            tempPastParClose++;
                                        }

                                        tempPastParClose--;

                                        if (parClose < input.Length - 2 && input[parClose + 1] == '(' && Char.IsDigit(input[parClose + 2]))
                                        {
                                            operandTwo = Convert.ToDouble(input.Substring(parClose + 2, (tempPastParClose - (parClose + 1))));
                                        }
                                        else if (parClose < input.Length - 1 && Char.IsDigit(input[parClose + 1]))
                                        {
                                            operandTwo = Convert.ToDouble(input.Substring(parClose + 1, (tempPastParClose - parClose)));
                                        }

                                        if (input[parClose + 1] == '(')
                                        {
                                            lenReplace = Convert.ToString(operandTwo).Length + (parClose - parOpen) + 3;
                                        }
                                        else
                                        {
                                            lenReplace = Convert.ToString(operandTwo).Length + (parClose - parOpen) + 1;
                                        }

                                    }


                                    // I forget what this does. I think I was having problems with operand or
                                    // operandTwo ending up with wrong values and this seems to have no problems.
                                    if (!operandGot)
                                    {
                                        operand = Convert.ToString(operandTwo);
                                    }

                                    // If replace is at index 0 and we have something in operand, solve for new operand.
                                    // If start is at 0, we don't have a negative before startReplace.
                                    if (startReplace == 0 && operand.Length > 0)
                                    {
                                        operand = Convert.ToString(operandOne * Convert.ToDouble(operand));
                                    }
                                    else
                                    {
                                        // If operandOne is negative. Account for that.
                                        if (startReplace > 0 && input[startReplace - 1] == '-')
                                        {
                                            if (startReplace == 1 || (!Char.IsDigit(input[startReplace - 2]) &&
                                                                        input[startReplace - 2] != ')'))
                                            {
                                                if (operand.Length > 0)
                                                {
                                                    operand = Convert.ToString((operandOne / -1) * Convert.ToDouble(operand));
                                                }
                                                else
                                                {
                                                    operand = Convert.ToString((operandOne / -1) * operandTwo);
                                                }

                                                startReplace--;

                                                if (lenReplace > 0)
                                                {
                                                    lenReplace++;
                                                }
                                            }
                                        }

                                        // Else just multiply opOne and opTwo.
                                        else
                                        {
                                            if (operand.Length > 0)
                                            {
                                                operand = Convert.ToString(operandOne * Convert.ToDouble(operand));
                                            }
                                            else
                                            {
                                                operand = Convert.ToString(operandOne * operandTwo);
                                            }
                                        }
                                    }

                                    // Insert parentheses around the substr to replace, to not cause problems with
                                    // the next loop.
                                    if (lenReplace == 0)
                                    {
                                        lenReplace = parClose - startReplace + 1;

                                        input = input.Insert(startReplace, "(");
                                        input = input.Insert((startReplace + lenReplace), ")");
                                    }
                                    else
                                    {
                                        input = input.Insert(startReplace, "(");
                                        input = input.Insert((startReplace + lenReplace + 1), ")");
                                    }

                                    startReplace++;
                                    endReplace++;

                                    ReplaceOp(ref lenReplace, startReplace, ref input, operand);

                                    indexCurOp = -1;
                                    curOp = 0;
                                    continue;
                                }


                                // Length of operands, operators, parenthses etc to replace.
                                lenReplace = parClose - startReplace + 1;

                                // Parentheses around replacement.
                                input = input.Insert(startReplace, "(");
                                input = input.Insert((startReplace + lenReplace), ")");

                                // Increment because we added parentheses.
                                startReplace++;

                                ReplaceOp(ref lenReplace, startReplace, ref input, operand);

                                indexCurOp = -1;
                                curOp = 0;
                                continue;
                            }

                            // Deals with negative signs. If inside of parOpen.
                            if (input[parOpen + 1] == '-')
                            {
                                // If double negative, remove both and parentheses.
                                if (parOpen > 0 && input[parOpen - 1] == '-')
                                {
                                    input = input.Remove(parClose, 1);
                                    input = input.Remove(parOpen - 1, 3);
                                }

                                // Just remove parentheses.
                                else
                                {
                                    input = input.Remove(parClose, 1);
                                    input = input.Remove(parOpen, 1);
                                }
                            }

                            // Otherwise if outside of parOpen just remove parentheses.
                            else if ((parOpen > 0 && input[parOpen - 1] == '-') || parOpen >= 0)
                            {
                                input = input.Remove(parClose, 1);
                                input = input.Remove(parOpen, 1);
                            }

                            indexCurOp = -1;
                            curOp = 0;
                            continue;
                        }

                        // Just solve regular expressions inside the parentheses pair.
                        else if (indexCurOp >= 0)
                        {
                            operandCounter = indexCurOp;

                            GetOp2(ref input, ref operandCounter, ref subLen, ref indexCurOp, ref operandTwo);

                            endReplace = operandCounter;

                            operandCounter = indexCurOp; // reset operandCounter and go left

                            GetOp1(ref operandCounter, input, ref subLen, ref operandOne, indexCurOp);

                            startReplace = operandCounter;

                            operandCounter = indexCurOp; // pointless

                            lenReplace = endReplace - startReplace + 1;

                            solveOperands(ref input, ref indexCurOp, ref operand,
                                operandOne, operandTwo, @decimal);

                            ReplaceOp(ref lenReplace, startReplace, ref input, operand);

                            indexCurOp = -1;
                            curOp = 0;
                            continue;
                        }
                    }
                }

                // No parentheses. Fix '-0'?
                else
                {
                    // Not sure if this is needed after fixing a few things but it doesn't hurt.
                    if (input.IndexOf(operators[0]) < 0 && input.IndexOf(operators[1]) < 0
                        && input.IndexOf(operators[2]) < 0 && input.IndexOf(operators[3]) < 0
                        && input.IndexOf(operators[4]) < 0)
                    {
                        if (input.IndexOf(operators[5]) == 0)
                        {
                            if (input.LastIndexOf(operators[5]) == 0)
                            {
                                if (input == "-0")
                                {
                                    input = "0";
                                }

                                A = input;
                                inputBox.Text = input;
                                return;
                            }
                        }
                        else if (input.IndexOf(operators[5]) < 0)
                        {
                            A = input;
                            inputBox.Text = input;
                            return;
                        }
                    }
                }


                // Finds an operator. Can be used to decide to remove a pair of parentheses.
                if (parOpen >= 0 && curOp < 6)
                {
                    indexCurOp = input.IndexOf((Convert.ToString(operators[curOp])), parOpen);
                }
                else if (curOp < 6)
                {
                    indexCurOp = input.IndexOf((Convert.ToString(operators[curOp])));
                }


                int iTemp = indexCurOp;


                // If indexCurOp is a negative sign, find the next valid minus sign.
                if (indexCurOp >= 0 && input[indexCurOp] == '-')
                {
                    while (indexCurOp > 0 && (!(Char.IsDigit(input[indexCurOp - 1])) &&
                            input[indexCurOp - 1] != ')'))
                    {
                        indexCurOp = input.IndexOf(Convert.ToString(operators[5]), indexCurOp + 1);
                    }

                    if (indexCurOp < 0 || indexCurOp > parClose)
                    {
                        indexCurOp = iTemp;
                    }
                }


                // if we are testing '-', no other operators after it so just set switchPrecOp to false.
                if (curOp + 1 == 5)
                {
                    switchPrecOp = -1;
                }
                else if (curOp >= 6)
                {
                    curOp = 0;
                    indexCurOp = -1;
                    continue;
                }

                // (*, /, and %) and also (+ and -) are same precedence so we have to make sure 
                // and do the first one of the pairs.
                else if (curOp < 5 && (operators[curOp] != '^'))
                {
                    GetPrec(ref parOpen, ref parClose, ref input, ref indexPrecOp,
                                operators, ref curOp, ref indexCurOp, ref switchPrecOp);
                }


                // If the current operator is outside of the parentheses pair, increment current operator to look for.
                if ((parClose >= 0 && parOpen >= 0 && indexCurOp > parClose) || indexCurOp < 0)
                {
                    if (curOp == 5)
                    {
                        curOp = 0;
                    }
                    else
                    {
                        curOp++;
                    }

                    indexCurOp = -1;
                    continue;
                }


                // If we changed indexCurOp because of precedence, set curOp to match.
                if (indexCurOp >= 0 && (input[indexCurOp] != operators[curOp]))
                {
                    curOp = operators.IndexOf(input[indexCurOp]);
                }


                // If current operator working with is '-' and it is at index 0, find next.
                if (curOp == 5)
                {
                    if (input[indexCurOp] == operators[curOp] && Char.IsDigit(input[indexCurOp + 1])
                        && indexCurOp == 0)
                    {
                        indexCurOp = input.IndexOf(Convert.ToString(operators[5]), indexCurOp + 1);
                    }
                }

                
                // Solve regular expression.

                operandCounter = indexCurOp;

                GetOp2(ref input, ref operandCounter, ref subLen, ref indexCurOp, ref operandTwo);

                endReplace = operandCounter;

                operandCounter = indexCurOp; // reset operandCounter and go left

                GetOp1(ref operandCounter, input, ref subLen, ref operandOne, indexCurOp);

                startReplace = operandCounter;

                operandCounter = indexCurOp; // pointless

                solveOperands(ref input, ref indexCurOp, ref operand,
                                operandOne, operandTwo, @decimal);

                lenReplace = endReplace - startReplace + 1;

                ReplaceOp(ref lenReplace, startReplace, ref input, operand);

                indexCurOp = -1;
                curOp = 0;
            } // end for


            // Just in case...
            inputBox.Text = input;
            A = inputBox.Text;
            return;
        }


        // Replace subtstr with solved expression.
        private void ReplaceOp(ref int lenReplace, int startReplace,
                                ref string input, string operand)
        {
            input = input.Remove(startReplace, lenReplace);
            input = input.Insert(startReplace, operand);
        }


        // Solves regular expressions based on the operator we pass.
        private void solveOperands(ref string input, ref int indexCurOp, ref string operand,
                                    double operandOne, double operandTwo, int @decimal)
        {
            switch (input[indexCurOp])
            {
                case '^':
                    operand = Convert.ToString(Math.Pow(operandOne, operandTwo)); // pointless
                    break;

                case '*':
                    operand = Convert.ToString(operandOne * operandTwo);
                    break;

                case '/':
                    operand = Convert.ToString(operandOne / operandTwo);
                    break;

                case '%':
                    operand = Convert.ToString(operandOne % operandTwo);
                    break;

                case '+':
                    operand = Convert.ToString(operandOne + operandTwo);
                    break;

                case '-':
                    operand = Convert.ToString(operandOne - operandTwo);
                    break;
            }

            // find the decimal point in the new operand.
            @decimal = operand.IndexOf('.');

            /* Just uncomment this out if you want to 'round' to certain amount of decimal places.
             * Currently rounds to 2 places.
             * 
            // if decimal is found, cut off at 2 decimal places. And if the two remaining places 
            // are 0, just erase.
            if (@decimal >= 0)
            {
                int decLen = operand.Length - @decimal - 1;

                if (decLen > 2)
                {
                    operand = operand.Remove(@decimal + 3);
                }
                if (operand[@decimal + 1] == '0' && operand[@decimal + 2] == '0')
                {
                    operand = operand.Remove(@decimal);
                }
            } */
        }


        // Checks for precedence of same level operators.
        private void GetPrec(ref int parOpen, ref int parClose, ref string input, ref int indexPrecOp,
                                string operators, ref int curOp, ref int indexCurOp, ref int switchPrecOp)
        {
            List<int> tempHighPrec = new List<int>(new int[3]);
            List<int> tempLowPrec = new List<int>(new int[2]);

            // No parentheses.
            // Inserts the indexes of the high level operators into a list.
            // Then checks if they are -1 -- don't exist and removes if true.
            // Then sets indexCurOp to the min of the remaining existing operators.

            // If low level operators, just sets indexCurOp and indexPrecOp to first index of each.
            // Solves for correct precedence after.
            if (parOpen < 0)
            {
                if (input[0] == '-')
                {
                    indexPrecOp = input.IndexOf((Convert.ToString(operators[curOp + 1])), 1);
                }
                else
                {
                    if (curOp == 1 || curOp == 2 || curOp == 3)
                    {
                        for (int g = 0; g < tempHighPrec.Count; g++)
                        {
                            tempHighPrec[g] = input.IndexOf(Convert.ToString(operators[g + 1]));
                        }

                        for (int g = 0; g < tempHighPrec.Count; g++)
                        {
                            if (tempHighPrec[g] < 0)
                            {
                                tempHighPrec.RemoveAt(g);
                                g--;
                            }
                        }

                        for (int g = 0; g < tempHighPrec.Count; g++)
                        {
                            if (g + 1 != tempHighPrec.Count)
                            {
                                indexCurOp = Math.Min(tempHighPrec[g], tempHighPrec[g + 1]);
                            }

                            if (tempHighPrec.Count == 1)
                            {
                                indexCurOp = tempHighPrec[0];
                            }
                        }

                        switchPrecOp = -1;
                    }
                    else
                    {
                        for (int g = 0; g < tempLowPrec.Count; g++)
                        {
                            tempLowPrec[g] = input.IndexOf(Convert.ToString(operators[g + 4]));
                        }

                        switchPrecOp = 0;

                        indexCurOp = tempLowPrec[0];
                        indexPrecOp = tempLowPrec[1];
                    }
                }
            }

            // Parentheses.
            // Same as above but checking if operators are outside of parentheses.
            else if (parOpen >= 0)
            {
                if (curOp == 1 || curOp == 2 || curOp == 3)
                {
                    for (int g = 0; g < tempHighPrec.Count; g++)
                    {
                        tempHighPrec[g] = input.IndexOf(Convert.ToString(operators[g + 1]), parOpen);
                    }

                    for (int g = 0; g < tempHighPrec.Count; g++)
                    {
                        if (tempHighPrec[g] < 0 || tempHighPrec[g] > parClose || tempHighPrec[g] < parOpen)
                        {
                            tempHighPrec.RemoveAt(g);
                            g--;
                        }
                    }

                    for (int g = 0; g < tempHighPrec.Count; g++)
                    {
                        if (g + 1 != tempHighPrec.Count)
                        {
                            indexCurOp = Math.Min(tempHighPrec[g], tempHighPrec[g + 1]);
                        }

                        if (tempHighPrec.Count == 1)
                        {
                            indexCurOp = tempHighPrec[0];
                        }
                    }

                    switchPrecOp = -1;
                }
                else
                {
                    for (int g = 0; g < tempLowPrec.Count; g++)
                    {
                        tempLowPrec[g] = input.IndexOf(Convert.ToString(operators[g + 4]), parOpen);
                    }

                    switchPrecOp = 0;

                    indexCurOp = tempLowPrec[0];
                    indexPrecOp = tempLowPrec[1];

                    if (indexCurOp > parClose)
                    {
                        indexCurOp = -1;
                    }
                    if (indexPrecOp > parClose)
                    {
                        indexPrecOp = -1;
                    }
                }
            }

            // Takes min of indexCurOp and precOp, this only runs if we didn't set switch to
            // -1 because we were working with 3 high level operators.
            if (switchPrecOp >= 0)
            {
                switchPrecOp = Math.Min(indexCurOp, indexPrecOp);
            }

            // Sets indexCurOp to switchPrecOp if it has high precedence.
            if (switchPrecOp > 0)
            {
                if (input[switchPrecOp] != '-' && Char.IsDigit(input[switchPrecOp - 1]) &&
                    (Math.Min(indexCurOp, indexPrecOp) == indexPrecOp))
                {
                    indexCurOp = switchPrecOp;
                }
            }
        }


        // Gets operandTwo for a regular expression.
        // Loops forwards starting at indexCurOp until it reaches something other than the number.
        // Then stores the number and sets a substr length to extend once we solve for operandOne.
        private void GetOp2(ref string input, ref int operandCounter, ref int subLen,
                            ref int indexCurOp, ref double operandTwo)

        {
            operandCounter++;

            if (input[operandCounter] == '-')
            {
                operandCounter++;

                while (operandCounter >= 0 && operandCounter < input.Length &&
                        (Char.IsDigit(input[operandCounter]) || input[operandCounter] == '.'))
                {
                    operandCounter++;
                }
            }
            else
            {
                while (operandCounter >= 0 && operandCounter < input.Length &&
                        (Char.IsDigit(input[operandCounter]) || input[operandCounter] == '.'))
                {
                    operandCounter++;
                }
            }


            
            if (operandCounter >= input.Length)
            {
                operandCounter = input.Length - 1;
            }
            else
            {
                operandCounter--;
            }

            subLen = operandCounter - indexCurOp;

            operandTwo = Convert.ToDouble(input.Substring(indexCurOp + 1, subLen));
        }


        // Solves for operandOne in the same way as GetOp2().
        private void GetOp1(ref int operandCounter, string input, ref int subLen,
                            ref double operandOne, int indexCurOp)
        {
            operandCounter--;

            // Going left.
            while (operandCounter >= 0 && operandCounter < input.Length &&
                    (Char.IsDigit(input[operandCounter]) || input[operandCounter] == '.'))
            {
                operandCounter--;
            }


            if (operandCounter < 0)
            {
                operandCounter = 0;
            }
            else
            {
                if (input[operandCounter] == '-')
                {
                    if (operandCounter != 0 && Char.IsDigit(input[operandCounter - 1]))
                    {
                        operandCounter++;
                    }
                }
                else
                {
                    operandCounter++;
                }
            }

            subLen = indexCurOp - operandCounter;

            operandOne = Convert.ToDouble(input.Substring(operandCounter, subLen));
        }


        private void button0_Click(object sender, EventArgs e)
        {
            inputBox.Text += '0';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            inputBox.Text += '1';
        }

        private void button2_Click(object sender, EventArgs e)
        {
            inputBox.Text += '2';
        }

        private void button3_Click(object sender, EventArgs e)
        {
            inputBox.Text += '3';
        }

        private void button4_Click(object sender, EventArgs e)
        {
            inputBox.Text += '4';
        }

        private void button5_Click(object sender, EventArgs e)
        {
            inputBox.Text += '5';
        }

        private void button6_Click(object sender, EventArgs e)
        {
            inputBox.Text += '6';
        }

        private void button7_Click(object sender, EventArgs e)
        {
            inputBox.Text += '7';
        }

        private void button8_Click(object sender, EventArgs e)
        {
            inputBox.Text += '8';
        }

        private void button9_Click(object sender, EventArgs e)
        {
            inputBox.Text += '9';
        }

        private void butOpenP_Click(object sender, EventArgs e)
        {
            inputBox.Text += '(';
        }

        private void butCloseP_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += ')';
            }
        }

        private void butPow_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += '^';
            }
        }

        private void butMult_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += '*';
            }
        }

        private void butDiv_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += '/';
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += '+';
            }
        }

        private void butSub_Click(object sender, EventArgs e)
        {
            inputBox.Text += '-';
        }

        private void delete_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text = inputBox.Text.Remove(inputBox.Text.Length - 1);
            }
            if (inputBox.Text.Length == 0)
            {
                warningLbl.Text = "";
            }
        }

        private void butAns_Click(object sender, EventArgs e)
        {
            inputBox.Text += A;
        }

        private void butClear_Click(object sender, EventArgs e)
        {
            inputBox.Text = "";
            warningLbl.Text = "";
        }

        private void butPi_Click(object sender, EventArgs e)
        {
            inputBox.Text += "Pi";
        }

        private void butSq_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += "^2";
            }
        }

        private void frmCalc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                inputBox.Text = "";
            }
            else if (e.KeyCode == Keys.Enter)
            {
                solve.PerformClick();
            }
        }

        private void quadraticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Quadratic quad = new Quadratic();

            Hide();
            quad.ShowDialog();
            Close();

        }

        private void frmCalc_Load(object sender, EventArgs e)
        {
            warningLbl.BackColor = Color.Transparent;
        }

        private void frmCalc_FormClosed(object sender, EventArgs e)
        {
            Close();
        }

        private void Quadratic_FormClosed(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPowX_Click(object sender, EventArgs e)
        {
            if (inputBox.Text.Length > 0)
            {
                inputBox.Text += "^";
            }
        }

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            inputBox.Text += "<>(";
        }

        private void btnDecimal_Click(object sender, EventArgs e)
        {
            inputBox.Text += '.';
        }

        private void btnSin_Click(object sender, EventArgs e)
        {
            if (btnSin.Text == "sin")
            {
                inputBox.Text += "sin(";
            }
            else
            {
                inputBox.Text += "asin(";
            }
        }

        private void btnCos_Click(object sender, EventArgs e)
        {
            if (btnCos.Text == "cos")
            {
                inputBox.Text += "cos(";
            }
            else
            {
                inputBox.Text += "acos(";
            }
        }

        private void btnTan_Click(object sender, EventArgs e)
        {
            if (btnTan.Text == "tan")
            {
                inputBox.Text += "tan(";
            }
            else
            {
                inputBox.Text += "atan(";
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            inputBox.Text += "log(";
        }

        private void btnLn_Click(object sender, EventArgs e)
        {
            inputBox.Text += "ln(";
        }

        private void quadraticToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Quadratic quad = new Quadratic();

            Hide();
            quad.ShowDialog();
            Close();
        }

        private void frmCalc_Shown(object sender, EventArgs e)
        {
            inputBox.Focus();
        }

        private void btnNRoot_Click(object sender, EventArgs e)
        {
            frmRoot root = new frmRoot();
            root.ShowDialog();

            inputBox.Text += root.GetRoot;
        }

        private void btnDegRad_Click(object sender, EventArgs e)
        {
            if (btnDegRad.Text == "DEG")
            {
                btnDegRad.Text = "RAD";
                deg = false;
            }
            else
            {
                btnDegRad.Text = "DEG";
                deg = true;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            inputBox.Text += "e";
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            inputBox.Text += "%";
        }

        private void btnLogN_Click(object sender, EventArgs e)
        {
            frmLog log = new frmLog();
            log.ShowDialog();

            inputBox.Text += log.GetLog;
        }

        private void btnInverse_Click(object sender, EventArgs e)
        {
            if (btnSin.Text == "sin")
            {
                btnSin.Text = "asin";
            }
            else
            {
                btnSin.Text = "sin";
            }

            if (btnCos.Text == "cos")
            {
                btnCos.Text = "acos";
            }
            else
            {
                btnCos.Text = "cos";
            }

            if (btnTan.Text == "tan")
            {
                btnTan.Text = "atan";
            }
            else
            {
                btnTan.Text = "tan";
            }
        }
    }
}