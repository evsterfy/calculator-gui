Just a project I am testing myself on. The C++ console version probably won't be updated any further.

If you want, you can try out the .exe and try to break the calculator and let me know what equation you tried.
I don't have the time and will power to try too many equations.

Currently supports roots, logs, sin/cos/tan, e, Pi, and implicit multiplication.